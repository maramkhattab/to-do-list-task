package com.sumerge.todolist.beans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;  

@ManagedBean
@RequestScoped  
public class ListItem {
	private int id;
	@NotNull
	private String title; 
    private String description;
	@Future
	@NotNull(message = "Date can't be empty")
    private Date dueDate;
    private String priority;
    private boolean done;
    private boolean deleted;
	private Map<String,Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();  
    List<String> priorityOptions;
    Connection connection; 
    
	public ListItem() {
		priorityOptions= new ArrayList<String>();
		priorityOptions.add("Low");
		priorityOptions.add("Medium");
		priorityOptions.add("High");
		
		
		
	}
	
	public List<String> getPriorityOptions() {
		return priorityOptions;
	}

	public void setPriorityOptions(List<String> priorityOptions) {
		this.priorityOptions = priorityOptions;
	}

	public Connection getConnection(){  
		try{  
		Class.forName("com.mysql.jdbc.Driver");     
		connection = DriverManager.getConnection( "jdbc:mysql://localhost:3306/todolistdb?useSSL=false&useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=GMT","root","Sumerge@2020");  
		}catch(Exception e){  
		System.out.println(e);  
		}  
		return connection;  
	}  

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public boolean isDone() {
		return done;
	}
	public void setDone(boolean done) {
		this.done = done;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public boolean isFormValid() {	
		
		
		boolean isValid=true;
		List <String> errors= new ArrayList<>();
		if(title==null )
		{	System.out.println("title missing");
			errors.add("Title is missing");
			isValid=false;
		}
		if(dueDate==null )
		{	System.out.println("Date is missing");
			errors.add("Date is missing");
			isValid=false;
		}
		
		
		Date todaysDate= new Date(); 
		if (!todaysDate.before(dueDate)) 
		{
			 System.out.println("Date in the past"); 
			 errors.add("Date must be in the future");
			isValid=false;
		}
		Errors errorMessages=new Errors();
		errorMessages.setErrorMessages(errors);
		sessionMap.put("errorMessages", errorMessages);
		
		return isValid;
	}

	public String markAsDone(int id) {
		try{  
			int result;
			connection = getConnection();  
			PreparedStatement stmt = connection.prepareStatement(  
			"UPDATE list_items SET done=? WHERE id=?;");  
			stmt.setBoolean(1, true);
			stmt.setInt(2, id);
			
			result = stmt.executeUpdate();  
			connection.close();  
		}catch(Exception e){  
			System.out.println(e);  
		} 
		return "homepage.xhtmlxhtml?faces-redirect=true";  
	}
	
	public String edit(ListItem item) {
		
		sessionMap.put("editItem",item);
		return "edit.xhtml?faces-redirect=true";  
		
	}
	
	public String update( ListItem item) {
		int result = 0;  
		if (! this.isFormValid())
			return "error.xhtml?faces-redirect=true";  
		
		try{  
			connection = getConnection();  
			PreparedStatement stmt = connection.prepareStatement("UPDATE list_items SET title=?, description=?, due_date=?, priority=?,done=?, deleted=? WHERE id=?;");  
			stmt.setString(1, title);  
			stmt.setString(2, description);  
			stmt.setDate(3, new java.sql.Date(dueDate.getTime()));  
			stmt.setString(4, priority);  
			stmt.setBoolean(5, false);
			stmt.setBoolean(6, false);
			stmt.setInt(7, id);
		
			
			result = stmt.executeUpdate();  
			connection.close();  
		}catch(Exception e){  
			System.out.println(e);  
		}  
		if(result !=0)  
			return "homepage.xhtml?faces-redirect=true";  
		else
			return "edit.xhtml?faces-redirect=true";  
	}

	public String delete(int id) {
		try{  
			int result;
			connection = getConnection();  
			PreparedStatement stmt = connection.prepareStatement(  
			"UPDATE list_items SET deleted=? WHERE id=?;");  
			stmt.setBoolean(1, true);
			stmt.setInt(2, id);
			
			result = stmt.executeUpdate();  
			connection.close();  
		}catch(Exception e){  
			System.out.println(e);  
		} 
		return "homepage.xhtmlxhtml?faces-redirect=true";  
		
	}
	
	public String save(){  
		int result = 0;  
		if (! this.isFormValid())
			return "error.xhtml?faces-redirect=true";  
		
		try{  
			connection = getConnection();  
			PreparedStatement stmt = connection.prepareStatement(  
			"insert into list_items (title,description,due_date,priority,deleted,done) values(?,?,?,?,?,?)");  
			stmt.setString(1, title);  
			stmt.setString(2, description);  
			stmt.setDate(3, new java.sql.Date(dueDate.getTime()));  
			stmt.setString(4, priority);  
			stmt.setBoolean(5, false);
			stmt.setBoolean(6, false);
		
			
			result = stmt.executeUpdate();  
			connection.close();  
		}catch(Exception e){  
			System.out.println(e);  
		}  
		if(result !=0)  
			return "homepage.xhtml?faces-redirect=true";  
		else
			return "create.xhtml?faces-redirect=true";  
	}  
	

}
