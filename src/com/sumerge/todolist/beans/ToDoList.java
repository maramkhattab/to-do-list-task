package com.sumerge.todolist.beans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class ToDoList {
	
	ArrayList<ListItem> toDoList ;  
	ArrayList<ListItem> completed;

	Connection connection;
	
	public ToDoList() {
		
	}
	
	public Connection getConnection(){  
		try{  
		Class.forName("com.mysql.jdbc.Driver");     
		connection = DriverManager.getConnection( "jdbc:mysql://localhost:3306/todolistdb?useSSL=false&useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=GMT","root","Sumerge@2020");  
		}catch(Exception e){  
		System.out.println(e);  
		}  
		return connection;  
	}  
	
	public ArrayList getToDoList() {
		try{  
			toDoList = new ArrayList();  
			connection = getConnection();  
			Statement stmt=getConnection().createStatement();    
			ResultSet rs=stmt.executeQuery("select * from list_items where deleted=0 and done=0");    
			
			while(rs.next()){  
			ListItem item = new ListItem();  
			item.setId(rs.getInt("id"));  
			item.setTitle(rs.getString("title"));  
			item.setDescription(rs.getString("description"));  
			item.setDueDate(rs.getDate("due_date"));  
			item.setPriority(rs.getString("priority"));  
			
			toDoList.add(item);  
			}  
			connection.close();          
			}catch(Exception e){  
			System.out.println(e);  
			} 
	
		return toDoList;
	}

	public void setToDoList(ArrayList toDoList) {
		this.toDoList = toDoList;
	}
	
	public ArrayList getCompleted() {
		try{  
			completed = new ArrayList();  
			connection = getConnection();  
			Statement stmt=getConnection().createStatement();    
			ResultSet rs=stmt.executeQuery("select * from list_items where done=1 and deleted=0"); 
		
			
			
			while(rs.next()){  
			ListItem item = new ListItem();  
			item.setId(rs.getInt("id"));  
			item.setTitle(rs.getString("title"));  
			item.setDescription(rs.getString("description"));  
			item.setDueDate(rs.getDate("due_date"));  
			item.setPriority(rs.getString("priority"));  
			
			completed.add(item);  
			}
			connection.close();    
		       
			}catch(Exception e){  
			System.out.println(e);  
			} 
	
		return completed;
	}

	
	public void setCompleted(ArrayList completed) {
		this.completed = completed;
	}  
	
	
}
